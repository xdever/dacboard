PCBNEW-LibModule-V1  2013. júl.  3., szerda, 11.00.04 CEST
# encoding utf-8
Units mm
$INDEX
2TAP
$EndINDEX
$MODULE 2TAP
Po 0 0 0 15 51D3E80C 00000000 ~~
Li 2TAP
Sc 0
AR 
Op 0 0 0
T0 0 -2.325 1.5 1.5 0 0.15 N I 21 N "2TAP"
T1 0 -1.175 0.5 0.5 0 0.125 N V 21 N "VAL**"
$PAD
Sh "1" R 1.2 1.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -0.8 0
$EndPAD
$PAD
Sh "2" R 1.2 1.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.8 0
$EndPAD
$EndMODULE 2TAP
$EndLIBRARY
